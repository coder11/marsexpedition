namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// ���������� � ������� ������� � csv ����� c �������� ������������
    /// </summary>
    class SurveyFileColumnMapper : ISurveyFileColumnMapper
    {
        /// <summary>
        /// �������� ����� �������, ���������� ��� �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        public int GetNameColumnIndex()
        {
            return 0;
        }

        /// <summary>
        /// �������� ����� �������, ���������� ���� �������� �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        public int GetBirthDateColumnIndex()
        {
            return 1;
        }

        /// <summary>
        /// �������� ����� �������, ���������� email �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        public int GetEmailColumnIndex()
        {
            return 2;
        }

        /// <summary>
        /// �������� ����� �������, ���������� ����� ���������� �������� �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        public int GetPhoneNumberColumnIndex()
        {
            return 3;
        }
    }
}