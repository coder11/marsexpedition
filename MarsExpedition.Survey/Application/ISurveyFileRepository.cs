﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MarsExpedition.Survey.Domain;

namespace MarsExpedition.Survey.Application
{
    /// <remarks>
    /// Не совсем репозиторий. Все операции сохранения транзакционны и работают без unit of work.
    /// </remarks>
    public interface ISurveyFileRepository
    {
        SurveyFile FindById(Guid id);
        void AddOrUpdate(SurveyFile file);
    }
}
