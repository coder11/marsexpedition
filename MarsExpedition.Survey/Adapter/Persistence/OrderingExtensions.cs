﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace MarsExpedition.Survey.Adapter.Persistence
{
    public static class OrderingExtensions
    {
        public static IQueryable<T> OrderByEx<T, TKey>(this IQueryable<T> source, bool forTheFirstTime, Expression<Func<T, TKey>> keySelector, bool isAscending)
        {
            if (forTheFirstTime)
            {
                return isAscending
                    ? source.OrderBy(keySelector)
                    : source.OrderByDescending(keySelector);
            }
            var oq = (IOrderedQueryable<T>) source;
            return isAscending
                    ? oq.ThenBy(keySelector)
                    : oq.ThenByDescending(keySelector);
        }
    }
}
