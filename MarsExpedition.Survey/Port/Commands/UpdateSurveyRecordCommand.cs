namespace MarsExpedition.Survey.Port.Commands
{
    /// <summary>
    /// ������� �� ���������� ������ ����������� �� ����� �� ����.
    /// ������ ����������� ��������� � ��������� ����������.
    /// </summary>
    public class UpdateSurveyRecordCommand
    {
        /// <summary>
        /// ������ ������
        /// </summary>
        public SurveyRecordDto SurveyRecord { get; set; }
    }
}