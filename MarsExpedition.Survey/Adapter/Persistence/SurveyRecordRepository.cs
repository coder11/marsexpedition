﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Security.AccessControl;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Domain;

namespace MarsExpedition.Survey.Adapter.Persistence
{
    public class SurveyRecordRepository : ISurveyRecordRepository
    {
        private readonly ISurveyDbContextFactory _factory;

        public SurveyRecordRepository(ISurveyDbContextFactory factory)
        {
            _factory = factory;
        }

        public void AddOrUpdate(SurveyRecord record)
        {
            using (var db = _factory.Create())
            {
                db.SurveyRecords.AddOrUpdate(record);
                db.SaveChanges();
            }
        }

        public void Delete(Guid recordId)
        {
            using (var db = _factory.Create())
            {
                var existing = db.SurveyRecords.Find(recordId);
                if (existing != null)
                {
                    db.Entry(existing).State = EntityState.Deleted;
                }
                db.SaveChanges();
            }
        }

        public SurveyRecord FindById(Guid recordId)
        {
            using (var db = _factory.Create())
            {
                return db.SurveyRecords.Find(recordId);
            }
        }

        private readonly Expression<Func<SurveyRecord, string>> _nameKeySelector = r => r.NormalizedName ?? r.Name;
        private readonly Expression<Func<SurveyRecord, string>> _phoneKeySelector = r => r.NormalizedMobilePhoneNumber ?? r.MobilePhoneNumber;
        private readonly Expression<Func<SurveyRecord, string>> _emailKeySelector = r => r.NormalizedEmail ?? r.Email;
        private readonly Expression<Func<SurveyRecord, DateTime?>> _birthDateKeySelector = r => r.NormalizedBirthDate;

        public IEnumerable<SurveyRecord> GetMany(out long resultsCount, IList<SurveyRecordSortOrder> sort = null, int count = 0, int skip = 0)
        {
            sort = sort ?? new List<SurveyRecordSortOrder>();
            if (!sort.Any())
            {
                sort.Add(new SurveyRecordSortOrder
                {
                    PropertyName = SurveyRecordSortOrder.NameColumn,
                    IsAscending = true
                });
            }

            using (var db = _factory.Create())
            {
                resultsCount = db.SurveyRecords.Count();

                var query = db.SurveyRecords.AsQueryable();
                var forTheFirstTime = true;
                foreach (var item in sort)
                {
                    query = AddQuerySortItem(forTheFirstTime, item, query);
                    forTheFirstTime = false;
                }

                var queryEnd = query.Skip(skip);
                if (count != 0)
                {
                    queryEnd = queryEnd.Take(count);
                }
                return queryEnd.ToList();
            }
        }

        private IQueryable<SurveyRecord> AddQuerySortItem(bool forTheFirstTime, SurveyRecordSortOrder item, IQueryable<SurveyRecord> query)
        {
            if (item.PropertyName == SurveyRecordSortOrder.NameColumn)
                return query.OrderByEx(forTheFirstTime, _nameKeySelector, item.IsAscending);
            if (item.PropertyName == SurveyRecordSortOrder.BirthDateColumn)
                return query.OrderByEx(forTheFirstTime, _birthDateKeySelector, item.IsAscending);
            if (item.PropertyName == SurveyRecordSortOrder.EmailColumn)
                return query.OrderByEx(forTheFirstTime, _emailKeySelector, item.IsAscending);
            if (item.PropertyName == SurveyRecordSortOrder.MobilePhoneNumberColumn)
                return query.OrderByEx(forTheFirstTime, _phoneKeySelector, item.IsAscending);

            throw new ArgumentOutOfRangeException();
        }
    }
}
