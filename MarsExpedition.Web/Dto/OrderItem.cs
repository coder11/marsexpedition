﻿namespace MarsExpedition.Web.Dto
{
    public class OrderItem
    {
        public const string DirAsc = "asc";
        public const string DirDesc = "desc";

        public int Column { get; set; }
        public string Dir { get; set; }
    }
}