using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// �������� ��������� ������������� ������ �������� �����������
    /// </summary>
    public class PhoneNumberNormalizer : IPhoneNumberNormalizer
    {
        private readonly Regex _separatorRegex = new Regex(@"[\s\-\(\)]+");

        private readonly Dictionary<Regex, string> _numberPatternsCountryCodesDict = new Dictionary<Regex, string>
        {
            { new Regex(@"^\+7(?<area>\d{3})(?<sn>\d{7})$"), "+7"},
            { new Regex(@"^8(?<area>\d{3})(?<sn>\d{7})$"), "+7"},
            { new Regex(@"^\+375(?<area>\d{2})(?<sn>\d{7})$"), "+375"},
            { new Regex(@"^0(?<area>\d{2})(?<sn>\d{7})$"), "+375"},
            { new Regex(@"^(?<area>\d{2})(?<sn>\d{7})$"), "+375"},
        };

        /// <summary>
        /// ����������� ������ ��������, ��������� �� ������ � ����� ����.
        /// ���� ������ �������� ������������� ����� - ���������� true, � ��������������� ��� � result.
        /// ���� ������ �������� ������������� ���������� - ���������� false. �������� result �� ����������.
        /// </summary>
        /// <remarks>
        /// ����������� ����� �������� � ������ E.123 "+[Country Code] [Area Code] [SN]"
        /// �������������� ��������� �������:
        /// ����������:
        /// +7xxxyyyyyyy
        /// +7(xxx)yyyyyyy
        /// 8xxxyyyyyyy
        /// 8(xxx)yyyyyyy
        /// 
        /// �����������
        /// +375xxyyyyyyy
        /// +375(xx)yyyyyyy
        /// 0(xx)yyyyyyy
        /// 0xxyyyyyyy
        /// xxyyyyyyy
        /// 
        /// �������������� ����������� ����� � ���� �������� � ����
        /// </remarks>
        /// <param name="mobilePhoneNumber">������ �������� �����������</param>
        /// <param name="result">��������������� ������ �������� � ������� ������� ������������</param>
        /// <returns>true, ���� ������ �������� ��� ������������, ����� false</returns>
        public bool TryNormalize(string mobilePhoneNumber, out string result)
        {
            result = null;
            mobilePhoneNumber = _separatorRegex.Replace(mobilePhoneNumber, String.Empty);

            foreach (var pattern in _numberPatternsCountryCodesDict)
            {
                var m = pattern.Key.Match(mobilePhoneNumber);
                if (m.Success)
                {
                    var code = pattern.Value;
                    var area = m.Groups["area"].Value;
                    var sn = m.Groups["sn"].Value;
                    result = $"{code} {area} {sn}";
                    return true;
                }
            }

            return false;
        }
    }
}