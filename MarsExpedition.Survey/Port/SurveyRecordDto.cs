using System;

namespace MarsExpedition.Survey.Port
{
    public class SurveyRecordDto : CreateSurveyRecordDto
    {
        public Guid Id { get; set; }
    }
}