﻿namespace MarsExpedition.Survey.Adapter.Persistence
{
    public class SurveyDbContextFactory : ISurveyDbContextFactory
    {
        public SurveyDbContext Create()
        {
            return new SurveyDbContext();
        }
    }
}