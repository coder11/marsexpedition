﻿using System.Data.Entity;
using MarsExpedition.Survey.Adapter.Persistence.Migrations;
using MarsExpedition.Survey.Domain;

namespace MarsExpedition.Survey.Adapter.Persistence
{
    public class SurveyDbContext : DbContext
    {
        public DbSet<SurveyFile> SurveyFiles { get; set; }
        public DbSet<SurveyRecord> SurveyRecords { get; set; }

        static SurveyDbContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SurveyDbContext, Configuration>());
        }

        public SurveyDbContext() : this("name=MarsEpxeditionSurveyConnectionString")
        {
        }

        public SurveyDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<SurveyFile>()
                .HasKey(x => x.Id)
                .HasMany(x => x.Records)
                    .WithOptional(x => x.SurveyFile)
                    .HasForeignKey(x => x.SurveyFileId);

            builder.Entity<SurveyRecord>()
                .HasKey(x => x.Id);
        }
    }
}