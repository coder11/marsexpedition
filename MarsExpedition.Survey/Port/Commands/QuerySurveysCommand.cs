using System.Collections.Generic;
using MarsExpedition.Survey.Application;

namespace MarsExpedition.Survey.Port.Commands
{
    /// <summary>
    /// ������� �� ������ ����� ������������ �� ����� �� ����
    /// </summary>
    public class QuerySurveysCommand
    {
        /// <summary>
        /// ������� ���������� ������. ���� ������ ������, ��� ����� null - ���������� �� ���������
        /// </summary>
        public IList<SurveyRecordSortOrder> Order { get; set; }

        /// <summary>
        /// ���������� �������, ������� ���� ��������. ���� 0 - ������� ��� ������
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// ���������� �������, ������� ���� ����������
        /// </summary>
        public int Skip { get; set; }
    }
}