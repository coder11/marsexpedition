﻿using MarsExpedition.Survey.Domain;

namespace MarsExpedition.Survey.Application
{
    /// <summary>
    /// Данные о поле по которому производится сортировка
    /// </summary>
    public class SurveyRecordSortOrder
    {
        public static string NameColumn = nameof(SurveyRecord.Name);
        public static string BirthDateColumn = nameof(SurveyRecord.BirthDate);
        public static string EmailColumn = nameof(SurveyRecord.Email);
        public static string MobilePhoneNumberColumn = nameof(SurveyRecord.MobilePhoneNumber);

        /// <summary>
        /// Имя поля по корому производится сортировка
        /// </summary>
        public string PropertyName { get; set; }

        /// <summary>
        /// true - по возрастанию, false - по убыванию
        /// </summary>
        public bool IsAscending { get; set; }

        /// <summary>
        /// Проверить валидность информации. Проверят, соотвествует ли название поля, доменной модели
        /// </summary>
        /// <returns>True, если все названия полей соотвествуют домененой модели, иначе false</returns>
        public bool Validate()
        {
            return PropertyName == NameColumn ||
                   PropertyName == BirthDateColumn ||
                   PropertyName == EmailColumn ||
                   PropertyName == MobilePhoneNumberColumn;
        }
    }
}