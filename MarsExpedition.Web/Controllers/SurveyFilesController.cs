﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using MarsExpedition.Survey.Port;
using MarsExpedition.Survey.Port.Commands;
using Newtonsoft.Json.Linq;

namespace MarsExpedition.Web.Controllers
{
    public class SurveyFilesController : ApiController
    {
        private readonly ISurveyApplication _application;

        public SurveyFilesController(ISurveyApplication application)
        {
            _application = application;
        }

        [HttpPost, Route("api/v1/upload")]
        public async Task UploadFile()
        {
            // TODO: swap dropzone with something wich can split file into parts and track progress
            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.Contents)
            {
                var size = file.Headers.ContentLength;
                var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                await file.ReadAsStreamAsync()
                    .ContinueWith(task =>
                        _application.Execute(new ProcessFileFromStreamCommand()
                        {
                            Size = size,
                            FileName = filename,
                            Stream = task.Result
                        }));
            }
        }
    }
}
