﻿using System;
using System.IO;
using System.IO.Abstractions;
using System.Threading.Tasks;
using Common.Logging;
using CsvHelper;
using CsvHelper.Configuration;
using JetBrains.Annotations;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Domain.Normalization;

namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// Обработчик файлов с данными анкет претендентов на полет на Марс
    /// Обрабатывает записи последовательно, сохраняя их в базу
    /// </summary>
    public class SurveyFileProcessor : ISurveyFileProcessor
    {
        private static readonly ILog _log = LogManager.GetLogger<SurveyFileProcessor>();

        private const string Delimiter = "\t";

        private readonly ISurveyFileColumnMapper _columnMapper;
        private readonly ISurveyFileRepository _fileRepository;
        private readonly ICsvFactory _csvFactory;
        private readonly INameNormalizer _nameNormalizer;
        private readonly IBirthDateNormalizer _birthDateNormalizer;
        private readonly IEmailNormalizer _emailNormalizer;
        private readonly IPhoneNumberNormalizer _phoneNumberNormalizer;

        public SurveyFileProcessor([NotNull] ISurveyFileColumnMapper columnMapper,
            [NotNull] ISurveyFileRepository fileRepository,
            [NotNull] ICsvFactory csvFactory,
            [NotNull] INameNormalizer nameNormalizer,
            [NotNull] IBirthDateNormalizer birthDateNormalizer,
            [NotNull] IEmailNormalizer emailNormalizer,
            [NotNull] IPhoneNumberNormalizer phoneNumberNormalizer)
        {
            if (columnMapper == null) throw new ArgumentNullException(nameof(columnMapper));
            if (fileRepository == null) throw new ArgumentNullException(nameof(fileRepository));
            if (csvFactory == null) throw new ArgumentNullException(nameof(csvFactory));
            if (nameNormalizer == null) throw new ArgumentNullException(nameof(nameNormalizer));
            if (birthDateNormalizer == null) throw new ArgumentNullException(nameof(birthDateNormalizer));
            if (emailNormalizer == null) throw new ArgumentNullException(nameof(emailNormalizer));
            if (phoneNumberNormalizer == null) throw new ArgumentNullException(nameof(phoneNumberNormalizer));

            _columnMapper = columnMapper;
            _fileRepository = fileRepository;
            _csvFactory = csvFactory;
            _nameNormalizer = nameNormalizer;
            _birthDateNormalizer = birthDateNormalizer;
            _emailNormalizer = emailNormalizer;
            _phoneNumberNormalizer = phoneNumberNormalizer;
        }

        /// <summary>
        /// Асинхронно обрабатывает файл
        /// </summary>
        /// <param name="file">Доменный объект файла с анкетными данными претендентов</param>
        /// <param name="stream">Открытый поток с данными из файла. Данный метод закроет поток</param>
        /// <returns>Таска с обработкой файла</returns>
        public async Task ProcessFileAsync(SurveyFile file, Stream stream)
        {
            if (file.ProcessingStatus == SurveyFileProcessingStatus.Error)
                throw new ApplicationException("File with processing status Error cannot be processed once more");

            if (file.ProcessingStatus == SurveyFileProcessingStatus.Done)
                throw new ApplicationException("Cannot process file for the second time");

            if (file.ProcessingStatus == SurveyFileProcessingStatus.Started)
            {
                if(!file.Size.HasValue)
                    throw new ApplicationException("Cannot continue to process file with unkown size");

                if(file.BytesProcessed >= stream.Length || file.BytesProcessed >= file.Size.Value)
                    throw new ApplicationException("Cannot contiune to process file, where already processed part seems to be larger than the whole file");

                if(!stream.CanSeek)
                    throw new ApplicationException("Cannot continue to process file, if the stream does not support seeking");

                stream.Seek(file.Size.Value, SeekOrigin.Begin);
            }

            file.ProcessingStatus = SurveyFileProcessingStatus.Started;
            _fileRepository.AddOrUpdate(file);

            await Task.Run(() =>
            {
                using (var sr = new StreamReader(stream))
                {
                    var csvReader = _csvFactory.CreateParser(sr, new CsvConfiguration()
                    {
                        CountBytes = true,
                        Encoding = sr.CurrentEncoding,
                        Delimiter = Delimiter,
                    });
                    DoProcessCsvFile(csvReader, file);
                }
            });
        }

        private void DoProcessCsvFile(ICsvParser csvReader, SurveyFile file)
        {
            _log.Debug($"Started processing file {file.Id}");
            for (;;)
            {
                try
                {
                    var row = csvReader.Read();
                    if (row == null)
                    {
                        file.ProcessingStatus = SurveyFileProcessingStatus.Done;
                        _fileRepository.AddOrUpdate(file);
                        OnFileProcessingUpdate(file);
                        _log.Debug($"Done processing file {file.Id}");
                        return;
                    }

                    var record = new SurveyRecord(
                        row[_columnMapper.GetNameColumnIndex()],
                        row[_columnMapper.GetBirthDateColumnIndex()],
                        row[_columnMapper.GetEmailColumnIndex()],
                        row[_columnMapper.GetPhoneNumberColumnIndex()]);

                    record.NormalizeName(_nameNormalizer);
                    record.NormalizeBirthDate(_birthDateNormalizer);
                    record.NormalizeMobilePhoneNumber(_phoneNumberNormalizer);
                    record.NormalizeEmail(_emailNormalizer);

                    file.AddRecord(record);
                    file.BytesProcessed = csvReader.BytePosition;
                    file.ProcessingStatus = SurveyFileProcessingStatus.Started;
                    _fileRepository.AddOrUpdate(file);
                    OnFileProcessingUpdate(file);
                }
                catch (Exception ex)
                {
                    _log.Warn($"Error while processing file {file.Id}", ex);
                    file.ProcessingStatus = SurveyFileProcessingStatus.Error;
                    _fileRepository.AddOrUpdate(file);
                    OnFileProcessingUpdate(file);
                    return;
                }
            }
        }

        /// <summary>
        /// Событие, которое должно вызывается при изменении состояния обработки файла
        /// </summary>
        public event EventHandler<FileProcessingUpdateEvent> FileProcessingUpdate;

        protected virtual void OnFileProcessingUpdate(SurveyFile file)
        {
            var @event = new FileProcessingUpdateEvent()
            {
                Progress = file.GetProgress(),
                Status = file.ProcessingStatus,
                SurveyFileId = file.Id
            };
            FileProcessingUpdate?.Invoke(this, @event);
        }
    }
}