﻿using System;
using System.Text.RegularExpressions;

namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// Алгоритма нормализатора email претендента
    /// </summary>
    public class EmailNormalizer : IEmailNormalizer
    {
        private readonly Regex _whiteSpaceRegex = new Regex(@"\s+");

        /// <summary>
        /// Нормализовать email, пришедший из анкеты в сыром виде.
        /// Если email нормализовать можно - должен возвращать true, с нормализованым email в result.
        /// Если email  нормализовать невозможно - должен возвращать false. Значение result не определено.
        /// </summary>
        /// <param name="email">Email претендента</param>
        /// <param name="result">Нормализованный email в слуачае удачной нормализации</param>
        /// <returns>true, если email был нормализован, иначе false</returns>
        /// <remarks>
        /// Убирает все лишние пробелы
        /// </remarks>
        public bool TryNormalize(string email, out string result)
        {
            result = _whiteSpaceRegex.Replace(email, String.Empty);
            return !result.Equals(email);
        }
    }
}