﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsExpedition.Web.Util
{
    public static class StringExtensions
    {
        public static string UpperFirst(this string source)
        {
            return source.Substring(0, 1).ToUpper() + source.Substring(1);
        }
    }
}
