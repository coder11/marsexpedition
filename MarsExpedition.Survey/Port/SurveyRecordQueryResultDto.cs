using System.Collections.Generic;

namespace MarsExpedition.Survey.Port
{
    public class SurveyRecordQueryResultDto
    {
        public long TotalCount { get; set; }
        public IEnumerable<SurveyRecordDto> SurveyRecords { get; set; }
    }
}