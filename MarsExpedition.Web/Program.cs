﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;

namespace MarsExpedition.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (WebApp.Start<Startup>(ConfigurationManager.AppSettings["baseurl"]))
            {
                Console.WriteLine("Web Server is running.");
                Console.WriteLine("Press any key to quit.");
                Console.ReadLine();
            }
        }
    }
}
