﻿namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// Статус обработки файла с анкетными данными претендентов на экспедицию на Марс 
    /// </summary>
    public enum SurveyFileProcessingStatus
    {
        /// <summary>
        /// Еще не обработан
        /// </summary>
        NotStarted,

        /// <summary>
        /// Обработан частично
        /// </summary>
        Started,

        /// <summary>
        /// Обработан полностью
        /// </summary>
        Done,

        /// <summary>
        /// Полностью обработать не удалось: во время обработки возникла ошибка. 
        /// </summary>
        Error,
    }
}