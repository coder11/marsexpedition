﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MarsExpedition.Survey.Domain.Lib;
using MarsExpedition.Survey.Domain.Normalization;

namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// Запись из анкеты претендента на полет на Марс
    /// </summary>
    public class SurveyRecord : Entity<Guid>
    {
        public const string CommonDateFormat = "dd.mm.yyyy";

        /// <remarks>
        /// Для всяких разных ORM
        /// </remarks>
        public SurveyRecord()
        {
            
        }

        public SurveyRecord([NotNull] string name, [NotNull] string birthDate, [NotNull] string email, [NotNull] string mobilePhoneNumber)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(birthDate)) throw new ArgumentNullException(nameof(birthDate));
            if (string.IsNullOrWhiteSpace(email)) throw new ArgumentNullException(nameof(email));
            if (string.IsNullOrWhiteSpace(mobilePhoneNumber)) throw new ArgumentNullException(nameof(mobilePhoneNumber));

            Name = name;
            BirthDate = birthDate;
            Email = email;
            MobilePhoneNumber = mobilePhoneNumber;
        }

        /// <summary>
        /// Нормализовать ФИО претендента. Если нормализовать имя удалось - то <see cref="NormalizedName"/> заполняется нормализованным значением. 
        /// В противном случае останется равным null
        /// </summary>
        /// <param name="normalizer">Алгоритм по которому будет нормализоваться ФИО</param>
        public void NormalizeName(INameNormalizer normalizer)
        {
            string result;
            NormalizedName = normalizer.TryNormalize(Name, out result) ? result : null;
        }

        /// <summary>
        /// Нормализовать дата рождения претендента. Если нормализовать имя удалось - то <see cref="NormalizedBirthDate"/> заполняется нормализованным значением. 
        /// В противном случае останется равным null
        /// </summary>
        /// <param name="normalizer">Алгоритм по которому будет нормализоваться дата рождения</param>
        public void NormalizeBirthDate(IBirthDateNormalizer normalizer)
        {
            DateTime result;
            NormalizedBirthDate = normalizer.TryNormalize(BirthDate, out result) ? result : (DateTime?) null;
        }

        /// <summary>
        /// Нормализовать дату рождения претендента. Если нормализовать имя удалось - то <see cref="NormalizeMobilePhoneNumber"/> заполняется нормализованным значением. 
        /// В противном случае останется равным null
        /// </summary>
        /// <param name="normalizer">Алгоритм по которому будет нормализоваться дата рождения</param>
        public void NormalizeMobilePhoneNumber(IPhoneNumberNormalizer normalizer)
        {
            string result;
            NormalizedMobilePhoneNumber = normalizer.TryNormalize(MobilePhoneNumber, out result) ? result : null;
        }

        /// <summary>
        /// Нормализовать email претендента. Если нормализовать email удалось - то <see cref="NormalizedEmail"/> заполняется нормализованным значением. 
        /// В противном случае останется равным null
        /// </summary>
        /// <param name="normalizer">Алгоритм по которому будет нормализоваться дата рождения</param>
        public void NormalizeEmail(IEmailNormalizer normalizer)
        {
            string result;
            NormalizedEmail = normalizer.TryNormalize(Email, out result) ? result : null;
        }

        /// <summary>
        /// Фамилия, имя, отчество пртендента, как было записано в анкете
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Дата рождения претендента, как было записано в анкете
        /// </summary>
        public string BirthDate { get; private set; }

        /// <summary>
        /// Email претендента, как было записано в анкете
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Мобильный телефон претендента, как было записано в анкете
        /// </summary>
        public string MobilePhoneNumber { get; private set; }

        /// <summary>
        /// Нормализованое ФИО претендента
        /// </summary>
        public string NormalizedName { get; set; }

        /// <summary>
        /// Нормализованая дата рождения претендента
        /// </summary>
        public DateTime? NormalizedBirthDate { get; set; }

        /// <summary>
        /// Нормализованый Мобильный телефон претендента
        /// </summary>
        public string NormalizedMobilePhoneNumber { get; set; }

        /// <summary>
        /// Нормализованый email претендента
        /// </summary>
        public string NormalizedEmail { get; set; }

        /// <remarks>
        /// Для entity framework'а
        /// </remarks>
        public Guid? SurveyFileId { get; set; }

        /// <remarks>
        /// Для entity framework'а
        /// </remarks>
        public virtual SurveyFile SurveyFile { get; set; }
    }
}
