﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using AutoMapper;
using MarsExpedition.Survey.Adapter.Persistence;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Domain;
using MarsExpedition.Survey.Infrastructure;
using MarsExpedition.Survey.Port;
using MarsExpedition.Survey.Port.Commands;
using NUnit.Framework;
using SimpleInjector;

namespace MarsExpedition.Survey.Tests.Acceptance
{
    [TestFixture]
    public class SurveyAcceptanceTests
    {
        private readonly Container _container;

        public SurveyAcceptanceTests()
        {
            _container = Ioc.MakeSurveyContainer();
            var config = new MapperConfiguration(cfg =>
                    cfg.AddProfile<SurveyMappingProfile>());

            _container.RegisterSingleton<IMapper>(config.CreateMapper());
            _container.Register<ISurveyFileRepository, SurveyFileRepository>(Lifestyle.Singleton);
            _container.Register<ISurveyRecordRepository, SurveyRecordRepository>(Lifestyle.Singleton);
            _container.RegisterSingleton<ISurveyDbContextFactory>(new SurveyDbContextFactory());
            _container.Verify();

            InitTestDataBase();
        }

        void InitTestDataBase()
        {
            using (var db = _container.GetInstance<ISurveyDbContextFactory>().Create())
            {
                db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction,
                    $"ALTER DATABASE [{db.Database.Connection.Database}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
                db.Database.Delete();
                db.Database.Initialize(true);
            }
        }

        [Test]
        public void UploadFile_EditRecord_QueryRecords_Scenario()
        {
            var app = _container.GetInstance<ISurveyApplication>();
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(CsvFile));
            app.Execute(new ProcessFileFromStreamCommand()
            {
                Stream = stream
            }).Wait();

            var surveys = app.Execute(new QuerySurveysCommand()
            {
                Count = 5, Skip = 1
            });

            Assert.That(surveys.SurveyRecords.Count(), Is.EqualTo(5));
            Assert.That(surveys.TotalCount, Is.EqualTo(7));
            var leonid = surveys.SurveyRecords.First(x => x.Name == "Леонид Леонидович Леонидов");
            Assert.That(leonid.BirthDate, Is.EqualTo(new DateTime(1990, 11, 3).ToString(SurveyRecord.CommonDateFormat)));
            Assert.That(leonid.Email, Is.EqualTo("leonidov@test.ru"));
            Assert.That(leonid.MobilePhoneNumber, Is.EqualTo("+7 930 5555555"));

            leonid.MobilePhoneNumber = "8 910 1111111";
            app.Execute(new UpdateSurveyRecordCommand()
            {
                SurveyRecord = leonid
            });

            app.Execute(new CreateSurveyRecordCommand()
            {
                SurveyRecord = new CreateSurveyRecordDto()
                {
                    Name = "Леонид 2",
                    BirthDate = DateTime.Now.Date.ToString(SurveyRecord.CommonDateFormat),
                    MobilePhoneNumber = "8 910 2222222",
                    Email = "lenya2@test.ru"
                }
            });

            var surveys2 = app.Execute(new QuerySurveysCommand());
            var leonid1 = surveys2.SurveyRecords.First(x => x.Name == "Леонид Леонидович Леонидов");
            Assert.That(leonid1.MobilePhoneNumber, Is.EqualTo(leonid.MobilePhoneNumber));
            var leonid2 = surveys2.SurveyRecords.FirstOrDefault(x => x.Name == "Леонид 2");
            Assert.That(leonid2, Is.Not.Null);
        }

        private const string CsvFile =
            "Иван Иванович Иванов\t         1.01.1990 \t ivanov@test.ru       \t+7 930 111 11 11\r\n" +
            "Петр Петрович Петров\t         2.11.1970 \t petrov@test.ru       \t+7 930 222 22 22\r\n" +
            "Петр Петрович Петров\t         2.11.1980 \t ppetrov@test.ru      \t+7 930 333 33 33\r\n" +
            "Петр Петрович Петров\t         2.11.1990 \t petr_petrov@test.ru  \t+7 930 444 44 44\r\n" +
            "Леонид Леонидович Леонидов\t   3.11.1990 \t leonidov@test.ru     \t+7 930 555 55 55\r\n" +
            "Алексей Алексеевич Алексеев\t  4.11.1990 \t alekseev@test.ru     \t+7 930 666 66 66\r\n" +
            "Дмитрий Дитриевич Дмитров\t    5.11.1990 \t dmitrov@test.ru      \t+7 930 777 77 77\r\n";
    }
}
