﻿namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// Интерфейс алгоритма нормализатора номера телефона претендента
    /// </summary>
    public interface IPhoneNumberNormalizer
    {
        /// <summary>
        /// Нормализовать номера телефона, пришедший из анкеты в сыром виде.
        /// Если номера телефона нормализовать можно - должен возвращать true, с нормализованным ФИО в result.
        /// Если номера телефона нормализовать невозможно - должен возвращать false. Значение result не определено.
        /// </summary>
        /// <param name="mobilePhoneNumber">Номера телефона претендента</param>
        /// <param name="result">Нормализованный номера телефона в слуачае удачной нормализации</param>
        /// <returns>true, если номера телефона был нормализован, иначе false</returns>
        bool TryNormalize(string mobilePhoneNumber, out string result);
    }
}