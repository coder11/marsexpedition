using System;
using System.IO;
using MarsExpedition.Survey.Domain;

namespace MarsExpedition.Survey.Port.Commands
{
    /// <summary>
    /// ������� �� ��������� ����� � ������� �����, ���������� � ���� ������
    /// </summary>
    public class ProcessFileFromStreamCommand
    {
        /// <summary>
        /// �������� ����� � ������� �� �����. �������� �� dispose'�� ����� �� ����������� �������
        /// </summary>
        public Stream Stream { get; set; }

        /// <summary>
        /// ������������ ��� �����. ����� ���� ������
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// ������ �����, ���� �� ������� ��������
        /// </summary>
        public long? Size { get; set; }

        /// <summary>
        /// Handler, ��� ������������ ��������� ���������
        /// </summary>
        public EventHandler<FileProcessingUpdateEvent> UploadProcessEventHandler { get; set; }
    }
}