using System;

namespace MarsExpedition.Survey.Port.Commands
{
    /// <summary>
    /// ������� �� �������� �������� ������ ����������� �� ����� �� ����
    /// </summary>
    public class DeleteSurveyRecordCommand
    {
        /// <summary>
        /// Id ������
        /// </summary>
        public Guid Id { get; set; }
    }
}