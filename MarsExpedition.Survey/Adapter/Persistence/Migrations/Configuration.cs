using System.Data.Entity.Migrations;

namespace MarsExpedition.Survey.Adapter.Persistence.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<SurveyDbContext>
    {
        public Configuration()
        {
            this.MigrationsDirectory = "Adapter.Persistance.Migrations";
            this.MigrationsNamespace = "MarsExpedition.Survey.Adapter.Persistence.Migrations";
            AutomaticMigrationsEnabled = true;
        }
    }
}
