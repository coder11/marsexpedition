﻿using System;

namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// Интерфейс алгоритма нормализатора даты рождения претендента
    /// </summary>
    public interface IBirthDateNormalizer
    {
        /// <summary>
        /// Нормализовать дату рождения, пришедшую из анкеты в сыром виде.
        /// Если дату рождения нормализовать можно - должен возвращать true, с нормализованой датой рождения в result.
        /// Если дату рождения нормализовать невозможно - должен возвращать false. Значение result не определено.
        /// </summary>
        /// <param name="birthDate">Дата рождения претендента</param>
        /// <param name="result">Нормализованная Дата рождения в слуачае удачной нормализации</param>
        /// <returns>true, если дата рождения была нормализована, иначе false</returns>
        bool TryNormalize(string birthDate, out DateTime result);
    }
}