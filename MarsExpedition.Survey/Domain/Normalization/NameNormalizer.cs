﻿using System.Linq;
using System.Text.RegularExpressions;

namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// Алгоритм нормализатора ФИО претендента
    /// </summary>
    public class NameNormalizer : INameNormalizer
    {
        private readonly Regex _whiteSpaceRegex = new Regex(@"\s+");

        /// <summary>
        /// Нормализует ФИО, пришедшее из анкеты в сыром виде.
        /// Если ФИО нормализовать можно возвращает true, с нормализованным ФИО в result.
        /// Если ФИО нормализовать невозможно - возвращает false. Значение result не определено.
        /// </summary>
        /// <remarks>
        /// Выполняет следующие преобразования над исходным ФИО:
        /// - проверяет, что данные только текстовые
        /// - убирает все пробельные символы, оставляя только единичный пробел между словами
        /// </remarks>
        /// <param name="name">ФИО претендента</param>
        /// <param name="result">Нормализованное ФИО в слуачае удачной нормализации</param>
        /// <returns>true, если фИО было нормализовано, иначе false</returns>
        public bool TryNormalize(string name, out string result)
        {
            if (name.Any(c => !char.IsLetter(c) && !char.IsWhiteSpace(c)))
            {
                result = name;
                return false;
            }

            result = _whiteSpaceRegex.Replace(name.Trim(), " ");
            return true;
        }
    }
}