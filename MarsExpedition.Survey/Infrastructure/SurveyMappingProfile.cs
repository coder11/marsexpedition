﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using MarsExpedition.Survey.Domain;
using MarsExpedition.Survey.Port;

namespace MarsExpedition.Survey.Infrastructure
{
    public class SurveyMappingProfile : Profile
    {
        public SurveyMappingProfile()
        {
            CreateMap<SurveyRecord, SurveyRecordDto>()
                .ForMember(x => x.Id, opt => opt.ResolveUsing(y => y.Id))
                .ForMember(x => x.Name, opt => opt.ResolveUsing(y => y.NormalizedName ?? y.Name))
                .ForMember(x => x.BirthDate, opt => opt.ResolveUsing(y => 
                    y.NormalizedBirthDate?.ToString(SurveyRecord.CommonDateFormat) ?? y.BirthDate))
                .ForMember(x => x.Email, opt => opt.ResolveUsing(y => y.NormalizedEmail ?? y.Email))
                .ForMember(x => x.MobilePhoneNumber, opt => opt.ResolveUsing(y => y.NormalizedMobilePhoneNumber ?? y.MobilePhoneNumber));
        }
    }
}
