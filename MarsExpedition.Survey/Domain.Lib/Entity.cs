﻿using System;

namespace MarsExpedition.Survey.Domain.Lib
{
    /// <summary>
    /// Доменная сущность
    /// </summary>
    /// <typeparam name="T">Тип ID сущности</typeparam>
    public class Entity<T>
    {
        /// <summary>
        /// Id доменной сущности
        /// </summary>
        public Guid Id { get; set; }

        public Entity()
        {
            Id = Guid.NewGuid();
        }
    }
}