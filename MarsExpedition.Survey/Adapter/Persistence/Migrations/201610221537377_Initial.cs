using System.Data.Entity.Migrations;

namespace MarsExpedition.Survey.Adapter.Persistence.Migrations
{
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SurveyFiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SourceFileName = c.String(),
                        SourceFilePath = c.String(),
                        BytesProcessed = c.Long(nullable: false),
                        Size = c.Long(),
                        ProcessingStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SurveyRecords",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        BirthDate = c.String(),
                        Email = c.String(),
                        MobilePhoneNumber = c.String(),
                        NormalizedName = c.String(),
                        NormalizedBirthDate = c.DateTime(),
                        NormalizedMobilePhoneNumber = c.String(),
                        SurveyFileId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SurveyFiles", t => t.SurveyFileId)
                .Index(t => t.SurveyFileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SurveyRecords", "SurveyFileId", "dbo.SurveyFiles");
            DropIndex("dbo.SurveyRecords", new[] { "SurveyFileId" });
            DropTable("dbo.SurveyRecords");
            DropTable("dbo.SurveyFiles");
        }
    }
}
