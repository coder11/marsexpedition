﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Abstractions;
using JetBrains.Annotations;
using MarsExpedition.Survey.Domain.Lib;

namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// Файл с анкетными данными претендентов на экспедицию на Марс 
    /// </summary>
    public class SurveyFile : Entity<Guid>
    {
        /// <remarks>
        /// Для всяких там ORM
        /// </remarks>
        public SurveyFile()
        {
            Records = new Collection<SurveyRecord>();
        }

        /// <summary>
        /// Создать доменный объект
        /// </summary>
        /// <param name="fileName">Опциональное имя файла</param>
        /// <param name="size">Размер файла, если он заранее известен</param>
        public SurveyFile(string fileName = null, long? size = null)
        {
            if(size.HasValue && size < 0)
                throw new ArgumentException("Size should be non-negative", nameof(size));

            Records = new Collection<SurveyRecord>();

            SourceFileName = string.IsNullOrWhiteSpace(fileName) ? null : fileName;
            Size = size;
            ProcessingStatus = SurveyFileProcessingStatus.NotStarted;
        }

        /// <summary>
        /// Создать доменный объект на основе файла с файловой системы
        /// </summary>
        /// <param name="filePath">Путь к файлу на файловой системе</param>
        /// <param name="fileSystem">Абстракция для файловой системы</param>
        /// <returns></returns>
        public static SurveyFile FromFileOnFileSystem([NotNull] string filePath, IFileSystem fileSystem)
        {
            var fi = fileSystem.FileInfo.FromFileName(filePath);
            if (!fi.Exists)
                throw new ArgumentException("File nof found", nameof(filePath));

            return new SurveyFile(fi.Name, fi.Length)
            {
                SourceFilePath = filePath
            };
        }

        /// <summary>
        /// Имя файла, содержащего данные из анкеты. Если имя файла неизвестно - null
        /// </summary>
        public string SourceFileName { get; private set; }

        /// <summary>
        /// Если файл может быть прочитан с машины, где запущена система - путь к файлу. Иначе - null
        /// </summary>
        public string SourceFilePath { get; private set; }

        /// <summary>
        /// Количество байтов, которые были распарсены, а полученные анкетные данные сохранены
        /// </summary>
        public long BytesProcessed { get; set; }

        /// <summary>
        /// Размер файла. Если размер неизвестен - null
        /// </summary>
        public long? Size { get; set; }

        /// <summary>
        /// Получить прогресс обработки файла в процентах
        /// </summary>
        /// <returns>Прогресс обработки файла в процентах</returns>
        public float GetProgress()
        {
            switch (ProcessingStatus)
            {
                case SurveyFileProcessingStatus.NotStarted:
                    return 0;
                case SurveyFileProcessingStatus.Started:
                    return Size.HasValue
                        ? Math.Max(BytesProcessed/Size.Value, 100)
                        : 0;
                case SurveyFileProcessingStatus.Done:
                case SurveyFileProcessingStatus.Error:
                    return 100;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Статус обработки файла
        /// </summary>
        public SurveyFileProcessingStatus ProcessingStatus { get; set; }

        /// <summary>
        /// Записи с анкетными данными претендентов на экспедицию на Марс из данного файла
        /// </summary>
        public virtual ICollection<SurveyRecord> Records { get; protected set; }

        /// <summary>
        /// Добавить пачку записей с анкетными данными претендентов на экспедицию на Марс из данного файла
        /// </summary>
        /// <param name="batch">Пачка записей</param>
        public void AddRecords(IList<SurveyRecord> batch)
        {
            foreach (var surveyRecord in batch)
            {
                Records.Add(surveyRecord);
            }
        }

        /// <summary>
        /// Добавить запись с анкетными данными претендентов на экспедицию на Марс из данного файла
        /// </summary>
        /// <param name="surveyRecord">Записей</param>
        public void AddRecord(SurveyRecord surveyRecord)
        {
            Records.Add(surveyRecord);
        }
    }
}
