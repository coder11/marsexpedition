﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MarsExpedition.Survey.Domain;

namespace MarsExpedition.Survey.Application
{
    /// <remarks>
    /// Не совсем репозиторий. Все операции сохранения транзакционны и работают без unit of work.
    /// </remarks>
    public interface ISurveyRecordRepository
    {
        /// <summary>
        /// Добавить или обновить запись с анкетными данными претендента
        /// </summary>
        /// <param name="record">Запись с анкетными данными претендента</param>
        void AddOrUpdate(SurveyRecord record);

        /// <summary>
        /// Получить Запись с анкетными данными претендента по ID
        /// </summary>
        /// <param name="recordId">ID записи</param>
        /// <returns>Запись с анкетными данными претендента если запист существует. Иначе null</returns>
        [CanBeNull]
        SurveyRecord FindById(Guid recordId);

        /// <summary>
        /// Удалить запись с анкетными данными претендента
        /// </summary>
        /// <param name="recordId">ID записи</param>
        void Delete(Guid recordId);

        // TODO: вынести resultsCount в отдельный метод, сделать через UnitOfWork
        /// <param name="resultsCount">Количество объектов, которые соотвествуют критерию поиска</param>
        /// <param name="sort">Список полей по которым произвести соритровку</param>
        /// <param name="count">Количество записей, которые надо получить. Если 0 - возвращает все записи</param>
        /// <param name="skip">Количество записей, которые надо пропустить</param>
        [NotNull]
        IEnumerable<SurveyRecord> GetMany(out long resultsCount, IList<SurveyRecordSortOrder> sort = null, int count = 0, int skip = 0);
    }
}
