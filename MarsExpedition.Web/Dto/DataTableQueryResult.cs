﻿using System.Collections.Generic;
using MarsExpedition.Survey.Port;

namespace MarsExpedition.Web.Dto
{
    public class DataTableQueryResult
    {
        public int Draw { get; set; }
        public long RecordsTotal { get; set; }
        public long RecordsFiltered { get; set; }
        public IEnumerable<SurveyRecordDto> Data { get; set; }
    }
}