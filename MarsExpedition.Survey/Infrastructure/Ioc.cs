﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using MarsExpedition.Survey.Domain;
using MarsExpedition.Survey.Domain.Normalization;
using MarsExpedition.Survey.Port;
using SimpleInjector;

namespace MarsExpedition.Survey.Infrastructure
{
    public static class Ioc
    {
        public static Container MakeSurveyContainer()
        {
            var container = new Container();
            container.Register<INameNormalizer, NameNormalizer>(Lifestyle.Singleton);
            container.Register<IPhoneNumberNormalizer, PhoneNumberNormalizer>(Lifestyle.Singleton);
            container.Register<IBirthDateNormalizer, BirthDateNormalizer>(Lifestyle.Singleton);
            container.Register<IEmailNormalizer, EmailNormalizer>(Lifestyle.Singleton);
            container.Register<ICsvFactory, CsvFactory>(Lifestyle.Singleton);
            container.Register<ISurveyFileColumnMapper, SurveyFileColumnMapper>(Lifestyle.Singleton);
            container.Register<ISurveyFileProcessor, SurveyFileProcessor>(Lifestyle.Singleton);
            container.Register<ISurveyApplication, SurveyApplication>(Lifestyle.Singleton);
            return container;
        }
    }
}
