﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using MarsExpedition.Survey.Adapter.Persistence;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Infrastructure;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Newtonsoft.Json.Serialization;
using Owin;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;

namespace MarsExpedition.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            HttpConfiguration config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional}
            );

            var physicalFileSystem = new PhysicalFileSystem(@".\Web"); //. = root, Web = your physical directory that contains all other static content, see prev step
            var options = new FileServerOptions
            {
                EnableDefaultFiles = true,
                FileSystem = physicalFileSystem
            };
            options.StaticFileOptions.FileSystem = physicalFileSystem;
            options.StaticFileOptions.ServeUnknownFileTypes = true;
            options.DefaultFilesOptions.DefaultFileNames = new[] { "index.html" }; //put whatever default pages you like here
            appBuilder.UseFileServer(options);

            var formatters = config.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            var container = Ioc.MakeSurveyContainer();
            var mapperConfig = new MapperConfiguration(cfg =>
                    cfg.AddProfile<SurveyMappingProfile>());

            container.RegisterSingleton<IMapper>(mapperConfig.CreateMapper());
            container.Register<ISurveyFileRepository, SurveyFileRepository>(Lifestyle.Singleton);
            container.Register<ISurveyRecordRepository, SurveyRecordRepository>(Lifestyle.Singleton);
            container.RegisterSingleton<ISurveyDbContextFactory>(new SurveyDbContextFactory());
            container.Verify();

            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            appBuilder.UseWebApi(config);
        }
    }
}
