namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// ��������� ��� ��������� ������� ������� �� csv �����
    /// </summary>
    public interface ISurveyFileColumnMapper
    {
        /// <summary>
        /// �������� ����� �������, ���������� ��� �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        int GetNameColumnIndex();

        /// <summary>
        /// �������� ����� �������, ���������� ���� �������� �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        int GetBirthDateColumnIndex();

        /// <summary>
        /// �������� ����� �������, ���������� email �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        int GetEmailColumnIndex();

        /// <summary>
        /// �������� ����� �������, ���������� ����� ���������� �������� �����������
        /// </summary>
        /// <returns>����� ��������</returns>
        int GetPhoneNumberColumnIndex();
    }
}