using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Domain;

namespace MarsExpedition.Survey.Adapter.Persistence
{
    public class SurveyFileRepository : ISurveyFileRepository
    {
        private readonly ISurveyDbContextFactory _factory;

        public SurveyFileRepository(ISurveyDbContextFactory factory)
        {
            _factory = factory;
        }

        public SurveyFile FindById(Guid fileId)
        {
            using (var db = _factory.Create())
            {
                return db.SurveyFiles.Include(x => x.Records).FirstOrDefault(x => x.Id == fileId);
            }
        }

        public void AddOrUpdate(SurveyFile file)
        {
            using (var db = _factory.Create())
            {
                db.SurveyFiles.AddOrUpdate(file);
                foreach (var surveyRecord in file.Records)
                {
                    surveyRecord.SurveyFileId = file.Id;
                    db.SurveyRecords.AddOrUpdate(surveyRecord);
                }
                db.SaveChanges();
            }
        }
    }
}