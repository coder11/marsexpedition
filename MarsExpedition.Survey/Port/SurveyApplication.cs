using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using JetBrains.Annotations;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Domain;
using MarsExpedition.Survey.Port.Commands;

namespace MarsExpedition.Survey.Port
{
    public class SurveyApplication : ISurveyApplication
    {
        private readonly ISurveyRecordRepository _surveyRecordRepository;
        private readonly ISurveyFileProcessor _surveyFileProcessor;
        private readonly ISurveyFileRepository _surveyFileRepository;
        private readonly IMapper _mapper;

        public SurveyApplication([NotNull] ISurveyRecordRepository surveyRecordRepository,
            [NotNull] ISurveyFileProcessor surveyFileProcessor,
            [NotNull] ISurveyFileRepository surveyFileRepository,
            [NotNull] IMapper mapper)
        {
            if (surveyRecordRepository == null) throw new ArgumentNullException(nameof(surveyRecordRepository));
            if (surveyFileProcessor == null) throw new ArgumentNullException(nameof(surveyFileProcessor));
            if (surveyFileRepository == null) throw new ArgumentNullException(nameof(surveyFileRepository));

            _surveyRecordRepository = surveyRecordRepository;
            _surveyFileProcessor = surveyFileProcessor;
            _surveyFileRepository = surveyFileRepository;
            _mapper = mapper;
        }

        public void Execute(CreateSurveyRecordCommand command)
        {
            var domainObject = new SurveyRecord(command.SurveyRecord.Name,
                command.SurveyRecord.BirthDate,
                command.SurveyRecord.Email,
                command.SurveyRecord.MobilePhoneNumber);
            _surveyRecordRepository.AddOrUpdate(domainObject);
        }

        public void Execute(UpdateSurveyRecordCommand command)
        {
            var domainObject = new SurveyRecord(command.SurveyRecord.Name,
                command.SurveyRecord.BirthDate,
                command.SurveyRecord.Email,
                command.SurveyRecord.MobilePhoneNumber);
            domainObject.Id = command.SurveyRecord.Id;
            _surveyRecordRepository.AddOrUpdate(domainObject);
        }

        public void Execute(DeleteSurveyRecordCommand command)
        {
            _surveyRecordRepository.Delete(command.Id);
        }

        public SurveyRecordQueryResultDto Execute(QuerySurveysCommand command)
        {
            var validation = command.Order?.All(x => x.Validate());
            if(validation.HasValue && !validation.Value)
                throw new ApplicationException("Invalid sort order info");

            long count;
            var queryResults = _surveyRecordRepository.GetMany(out count, command.Order, command.Count, command.Skip);
            return new SurveyRecordQueryResultDto
            {
                TotalCount = count,
                SurveyRecords = _mapper.Map<IEnumerable<SurveyRecordDto>>(queryResults)
            };
        }

        public Task Execute(ProcessFileFromStreamCommand command)
        {
            var domainOBject = new SurveyFile(command.FileName, command.Size);
            _surveyFileRepository.AddOrUpdate(domainOBject);
            if (command.UploadProcessEventHandler != null)
            {
                _surveyFileProcessor.FileProcessingUpdate += command.UploadProcessEventHandler;
            }

            return _surveyFileProcessor.ProcessFileAsync(domainOBject, command.Stream)
                .ContinueWith(task =>
                {
                    _surveyFileProcessor.FileProcessingUpdate -= command.UploadProcessEventHandler;
                });
        }
    }
}