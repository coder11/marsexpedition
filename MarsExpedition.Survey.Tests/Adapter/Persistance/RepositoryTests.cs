﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsExpedition.Survey.Adapter.Persistence;
using MarsExpedition.Survey.Domain;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace MarsExpedition.Survey.Tests.Adapter.Persistance
{
    [TestFixture]
    public class RepositoryTests
    {
        private SurveyDbContextFactory _factory;

        [SetUp]
        public void Setup()
        {
            _factory = new SurveyDbContextFactory();
            using (var db = _factory.Create())
            {
                db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, 
                    $"ALTER DATABASE [{db.Database.Connection.Database}] SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
                db.Database.Delete();
                db.Database.Initialize(true);
            }
        }

        [Test]
        public void When_entity_does_not_exist__repository_should_retrieve_just_stored_entity()
        {
            // Arrange
            var rep = new SurveyRecordRepository(_factory);
            var record = new SurveyRecord("name", "19.11.1988", "me@me.com", "+79001234567");

            // Act
            rep.AddOrUpdate(record);
            var record2 = rep.FindById(record.Id);

            // Assert
            Assert.That(record2.Id,                Is.EqualTo(record.Id));
            Assert.That(record2.Name,              Is.EqualTo(record.Name));
            Assert.That(record2.BirthDate,         Is.EqualTo(record.BirthDate));
            Assert.That(record2.Email,             Is.EqualTo(record.Email));
            Assert.That(record2.MobilePhoneNumber, Is.EqualTo(record.MobilePhoneNumber));
        }

        [Test]
        public void When_entity_exists__repository_should_update_existing_entity_to_new_value()
        {
            // Arrange
            var rep = new SurveyRecordRepository(_factory);
            var record = new SurveyRecord("name", "19.11.1988", "me@me.com", "+79001234567")
            {
                NormalizedName = "name"
            };

            // Act
            rep.AddOrUpdate(record);
            record.NormalizedName = "new-name";
            rep.AddOrUpdate(record);
            var record2 = rep.FindById(record.Id);

            // Assert
            Assert.That(record2.NormalizedName, Is.EqualTo(record.NormalizedName));
            Assert.That(record2.Name, Is.EqualTo(record.Name));
            Assert.That(record2.BirthDate, Is.EqualTo(record.BirthDate));
            Assert.That(record2.Email, Is.EqualTo(record.Email));
            Assert.That(record2.MobilePhoneNumber, Is.EqualTo(record.MobilePhoneNumber));
        }

        [Test]
        public void When_entity_exists__repository_should_delete_existing_record()
        {
            // Arrange
            var rep = new SurveyRecordRepository(_factory);
            var record = new SurveyRecord("name", "19.11.1988", "me@me.com", "+79001234567")
            {
                NormalizedName = "name"
            };

            // Act
            rep.AddOrUpdate(record);
            rep.Delete(record.Id);
            var record2 = rep.FindById(record.Id);

            // Assert
            Assert.That(record2, Is.Null);
        }

        [Test]
        public void When_entity_does_not_exist__repository_should_delete_nothing()
        {
            // Arrange
            var id = Guid.NewGuid();
            var rep = new SurveyRecordRepository(_factory);

            // Act
            rep.Delete(id);
            var record = rep.FindById(id);

            // Assert
            Assert.That(record, Is.Null);
        }

        [Test]
        public void Adding_records_to_survey_file_should_work_correctly()
        {
            // Arrange
            var file = new SurveyFile("whatever");
            var rep = new SurveyFileRepository(_factory);

            // Act
            rep.AddOrUpdate(file);
            file.AddRecord(new SurveyRecord("1", "1", "1", "1"));
            file.ProcessingStatus = SurveyFileProcessingStatus.Started;
            rep.AddOrUpdate(file);
            file.AddRecord(new SurveyRecord("2", "2", "2", "2"));
            file.AddRecord(new SurveyRecord("3", "3", "3", "3"));
            rep.AddOrUpdate(file);
            file.ProcessingStatus = SurveyFileProcessingStatus.Done;
            rep.AddOrUpdate(file);

            var file2 = rep.FindById(file.Id);

            // Assert
            Assert.That(file2.Records.Count, Is.EqualTo(3));
            Assert.That(file2.ProcessingStatus, Is.EqualTo(SurveyFileProcessingStatus.Done));
        }
    }
}
