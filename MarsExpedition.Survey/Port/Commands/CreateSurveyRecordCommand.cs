namespace MarsExpedition.Survey.Port.Commands
{
    /// <summary>
    /// ������� �� ���������� ������ ����������� �� ����� �� ����.
    /// ������ ����������� ��������� � ��������� ����������.
    /// </summary>
    public class CreateSurveyRecordCommand
    {
        /// <summary>
        /// ������ ������
        /// </summary>
        public CreateSurveyRecordDto SurveyRecord { get; set; }
    }
}