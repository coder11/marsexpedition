﻿namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// Интерфейс алгоритма нормализатора email претендента
    /// </summary>
    public interface IEmailNormalizer
    {
        /// <summary>
        /// Нормализовать email, пришедший из анкеты в сыром виде.
        /// Если email нормализовать можно - должен возвращать true, с нормализованым email в result.
        /// Если email  нормализовать невозможно - должен возвращать false. Значение result не определено.
        /// </summary>
        /// <param name="email">Email претендента</param>
        /// <param name="result">Нормализованный email в слуачае удачной нормализации</param>
        /// <returns>true, если email был нормализован, иначе false</returns>
        bool TryNormalize(string email, out string result);
    }
}