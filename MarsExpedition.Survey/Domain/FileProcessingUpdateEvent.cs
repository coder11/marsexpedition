using System;

namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// ������� ��������� ��������� ��������� ����� � ������� ����� ������������ �� ����� �� ����
    /// </summary>
    public class FileProcessingUpdateEvent
    {
        /// <summary>
        /// Id �����, � ������� ��������� ���������
        /// </summary>
        public Guid SurveyFileId { get; set; }
        
        /// <summary>
        /// ������ ��������� �����
        /// </summary>
        public SurveyFileProcessingStatus Status { get; set; }

        /// <summary>
        /// ������� �������� ��������� �����
        /// </summary>
        public float Progress { get; set; }
    }
}