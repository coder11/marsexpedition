﻿namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// Интерфейс алгоритма нормализатора ФИО претендента
    /// </summary>
    public interface INameNormalizer
    {
        /// <summary>
        /// Нормализовать ФИО, пришедшее из анкеты в сыром виде.
        /// Если ФИО нормализовать можно - должен возвращать true, с нормализованным ФИО в result.
        /// Если ФИО нормализовать невозможно - должен возвращать false. Значение result не определено.
        /// </summary>
        /// <param name="name">ФИО претендента</param>
        /// <param name="result">Нормализованное ФИО в слуачае удачной нормализации</param>
        /// <returns>true, если фИО было нормализовано, иначе false</returns>
        bool TryNormalize(string name, out string result);
    }
}