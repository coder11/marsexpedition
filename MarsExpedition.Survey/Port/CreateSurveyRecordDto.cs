namespace MarsExpedition.Survey.Port
{
    public class CreateSurveyRecordDto
    {
        public string Name { get; set; }
        public string BirthDate { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string Email { get; set; }
    }
}