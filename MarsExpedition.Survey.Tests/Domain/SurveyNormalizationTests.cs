﻿using System;
using System.Collections;
using MarsExpedition.Survey.Domain;
using MarsExpedition.Survey.Domain.Normalization;
using NUnit.Framework;

namespace MarsExpedition.Survey.Tests.Domain
{
    [TestFixture]
    public class SurveyNormalizationTests
    {
        [Test]
        public void Given_a_survey_with_incorrect_name__normalization_should_leave_name_as_is()
        {
            // Arrange
            var record = new SurveyRecord("!!74912874  garbage!!!!", "whatever", "whatever", "whatever");

            // Act
            record.NormalizeName(new NameNormalizer());

            // Assert
            Assert.That(record.NormalizedName, Is.Null);
        }

        [Test]
        [TestCase("  Мурыгин  Павел   Дмитриевич", "Мурыгин Павел Дмитриевич")]
        [TestCase("Мурыгин  Павел     Дмитриевич  ", "Мурыгин Павел Дмитриевич")]
        [TestCase("Мурыгин Павел Дмитриевич", "Мурыгин Павел Дмитриевич")]
        public void Given_a_survey_with_possibly_normalizable_name__normalization_should_change_name_to_expected_value(string name, string normalizedName)
        {
            // Arrange
            var record = new SurveyRecord(name, "whatever", "whatever", "whatever");

            // Act
            record.NormalizeName(new NameNormalizer());

            // Assert
            Assert.That(record.NormalizedName, Is.EqualTo(normalizedName));
        }

        [Test]
        [TestCaseSource(nameof(BirthDatePositiveTestCases))]
        public void Given_a_survey_with_possibly_normalizable_birh_date__normalization_should_change_birth_date_to_expected_value(string birthDate, DateTime normalizedBirthDate)
        {
            // Arrange
            var record = new SurveyRecord("whatever", birthDate, "whatever", "whatever");

            // Act
            record.NormalizeBirthDate(new BirthDateNormalizer());

            // Assert
            Assert.That(record.NormalizedBirthDate, Is.EqualTo(normalizedBirthDate));
        }

        [Test]
        [TestCase("any text")]
        [TestCase("32.12.2001")]
        [TestCase("1 просинеца 2001")]
        [TestCase("55 2001")]
        [TestCase("55 11 2001")]
        public void Given_a_survey_with_incorrect_birh_date__normalization_should_leave_normalized_birth_date_null(string birthDate)
        {
            // Arrange
            var record = new SurveyRecord("whatever", birthDate, "whatever", "whatever");

            // Act
            record.NormalizeBirthDate(new BirthDateNormalizer());

            // Assert
            Assert.That(record.NormalizedBirthDate, Is.Null);
        }

        [Test]
        [TestCase("+7 929 111 22 33", "+7 929 1112233")]
        [TestCase("+7(929)1112233", "+7 929 1112233")]
        [TestCase("+7(929)111-22-33", "+7 929 1112233")]
        [TestCase("+7 (929) 111-22-33", "+7 929 1112233")]
        [TestCase("8 929 111 22 33", "+7 929 1112233")]
        [TestCase("8(929)1112233", "+7 929 1112233")]
        [TestCase("8(929)111-22-33", "+7 929 1112233")]
        [TestCase("8 (929) 111-22-33", "+7 929 1112233")]
        [TestCase("+375 24 111 22 33", "+375 24 1112233")]
        [TestCase("+375241112233", "+375 24 1112233")]
        [TestCase("+375(24)1112233", "+375 24 1112233")]
        [TestCase("0 17 2334455", "+375 17 2334455")]
        [TestCase("17 2334455", "+375 17 2334455")]
        [TestCase("0(17)2-33-44-55", "+375 17 2334455")]
        [TestCase("0(17)233-44-55", "+375 17 2334455")]
        public void Given_a_survey_with_possibly_normalizable_phone_number__normalization_should_change_phone_number_to_expected_value(string phoneNumber, string normalizedPhoneNumber)
        {
            // Arrange
            var record = new SurveyRecord("whatever", "whatever", "whatever", phoneNumber);

            // Act
            record.NormalizeMobilePhoneNumber(new PhoneNumberNormalizer());

            // Assert
            Assert.That(record.NormalizedMobilePhoneNumber, Is.EqualTo(normalizedPhoneNumber));
        }

        [Test]
        [TestCase("!@!@ garbage@G")]
        [TestCase("11-22-33")]
        [TestCase("+7(929)111-22-xx")]
        public void Given_a_survey_with_incorrect_phone_number__normalization_should_leave_phone_number_as_is(string phoneNumber)
        {
            // Arrange
            var record = new SurveyRecord("whatever", "whatever", "whatever", phoneNumber);

            // Act
            record.NormalizeMobilePhoneNumber(new PhoneNumberNormalizer());

            // Assert
            Assert.That(record.NormalizedMobilePhoneNumber, Is.Null);
        }

        [Test]
        [TestCase("  ivan@ivanov@test.ru", "ivan@ivanov@test.ru")]
        [TestCase("iva  n@ivanov@ test.ru", "ivan@ivanov@test.ru")]
        [TestCase("  ivan@ivanov@te  st.ru  ", "ivan@ivanov@test.ru")]
        public void Given_a_survey_with_possibly_normalizable_email__normalization_should_update_normalized_email(string email, string normalizedEmail)
        {
            // Arrange
            var record = new SurveyRecord("whatever", "whatever", email, "whatever");

            // Act
            record.NormalizeEmail(new EmailNormalizer());

            // Assert
            Assert.That(record.NormalizedEmail, Is.EqualTo(normalizedEmail));
        }

        public static IEnumerable BirthDatePositiveTestCases
        {
            get
            {
                // the most canonical
                yield return new TestCaseData("19.11.1988", new DateTime(1988, 11, 19));

                // с "г", "года" на конце
                yield return new TestCaseData("19.11.1988г", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19.11.1988г.", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19.11.1988 г", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19.11.1988 г.", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19.11.1988 года", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19.11.1988 года.", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19 августа 1988 г", new DateTime(1988, 8, 19));
                yield return new TestCaseData("19 августа 1988 г.", new DateTime(1988, 8, 19));
                yield return new TestCaseData("19 авг 1988 г", new DateTime(1988, 8, 19));
                yield return new TestCaseData("19 авг 1988 г.", new DateTime(1988, 8, 19));
                yield return new TestCaseData("19.11.1988г ", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19.11.1988г. ", new DateTime(1988, 11, 19));

                // month before day (common for EN-US culture), when it's possible to figure out format
                yield return new TestCaseData("11.19.1988", new DateTime(1988, 11, 19));

                // partial data
                yield return new TestCaseData("1988", new DateTime(1988, 1, 1));
                yield return new TestCaseData("11.1988", new DateTime(1988, 11, 1));

                // different types of separators
                yield return new TestCaseData("19 11 1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19:11:1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19-11-1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19\\11\\1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19/11/1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("09.01.1988", new DateTime(1988, 01, 09));
                yield return new TestCaseData("9.1.1988", new DateTime(1988, 01, 09));
                yield return new TestCaseData("19.11.88", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19,11.1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("11,19.1988", new DateTime(1988, 11, 19));

                // as verbal
                yield return new TestCaseData("19 января 1988", new DateTime(1988, 1, 19));
                yield return new TestCaseData("19 янв 1988", new DateTime(1988, 1, 19));
                yield return new TestCaseData("19 янв. 1988", new DateTime(1988, 1, 19));
                yield return new TestCaseData("19 янв.1988", new DateTime(1988, 1, 19));
                yield return new TestCaseData("19 февраля 1988", new DateTime(1988, 2, 19));
                yield return new TestCaseData("19 фев 1988", new DateTime(1988, 2, 19));
                yield return new TestCaseData("19 февр 1988", new DateTime(1988, 2, 19));
                yield return new TestCaseData("19 марта 1988", new DateTime(1988, 3, 19));
                yield return new TestCaseData("19 мрт 1988", new DateTime(1988, 3, 19));
                yield return new TestCaseData("19 апреля 1988", new DateTime(1988, 4, 19));
                yield return new TestCaseData("19 апр 1988", new DateTime(1988, 4, 19));
                yield return new TestCaseData("19 мая 1988", new DateTime(1988, 5, 19));
                yield return new TestCaseData("19 июня 1988", new DateTime(1988, 6, 19));
                yield return new TestCaseData("19 июля 1988", new DateTime(1988, 7, 19));
                yield return new TestCaseData("19 августа 1988", new DateTime(1988, 8, 19));
                yield return new TestCaseData("19 авг 1988", new DateTime(1988, 8, 19));
                yield return new TestCaseData("19 сентября 1988", new DateTime(1988, 9, 19));
                yield return new TestCaseData("19 сент 1988", new DateTime(1988, 9, 19));
                yield return new TestCaseData("19 октября 1988", new DateTime(1988, 10, 19));
                yield return new TestCaseData("19 окт 1988", new DateTime(1988, 10, 19));
                yield return new TestCaseData("19 ноября 1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19 ноя 1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19 нбр 1988", new DateTime(1988, 11, 19));
                yield return new TestCaseData("19 декабря 1988", new DateTime(1988, 12, 19));
                yield return new TestCaseData("19 дек 1988", new DateTime(1988, 12, 19));
            }
        }

    }
}
