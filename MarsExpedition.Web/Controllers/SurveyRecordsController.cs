﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Port;
using MarsExpedition.Survey.Port.Commands;
using MarsExpedition.Web.Dto;
using MarsExpedition.Web.Util;

namespace MarsExpedition.Web.Controllers
{
    public class SurveyRecordsController : ApiController
    {
        private readonly ISurveyApplication _application;

        public SurveyRecordsController(ISurveyApplication application)
        {
            _application = application;
        }

        [HttpGet, Route("api/v1/survey_records")]
        public IHttpActionResult Get(int draw, int? start, int? length, [FromUri] List<Column> columns, [FromUri] List<OrderItem> order)
        {
            if (order.Any(x => x.Column >= columns.Count || x.Column < 0))
                return BadRequest("incorrect order");
            
            var cmd = new QuerySurveysCommand
            {
                Skip = start ?? 0,
                Count = length ?? 0,
                Order = order.Select(x => new SurveyRecordSortOrder
                {
                    IsAscending = x.Dir == OrderItem.DirAsc,
                    PropertyName = columns[x.Column].Data.UpperFirst()
                }).ToList()
            };

            var results = _application.Execute(cmd);
            var ret = new DataTableQueryResult()
            {
                Draw = draw,
                Data = results.SurveyRecords,
                RecordsFiltered = results.TotalCount,
                RecordsTotal = results.TotalCount,
            };
            return Ok(ret);
        }

        [HttpDelete, Route("api/v1/survey_records")]
        public IHttpActionResult Delete(string id)
        {
            Guid guid;
            if (!Guid.TryParse(id, out guid))
                return BadRequest("incorrect id, shoud be guid");

            _application.Execute(new DeleteSurveyRecordCommand()
            {
                Id = guid
            });
            return Ok();
        }

        [HttpPut, Route("api/v1/survey_records")]
        public void Edit(SurveyRecordDto record)
        {
            _application.Execute(new UpdateSurveyRecordCommand
            {
                SurveyRecord = record
            });
        }

        [HttpPost, Route("api/v1/survey_records")]
        public void Create(CreateSurveyRecordDto record)
        {
            _application.Execute(new CreateSurveyRecordCommand
            {
                SurveyRecord = record
            });
        }
    }
}