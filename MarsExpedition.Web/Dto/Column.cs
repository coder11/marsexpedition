﻿namespace MarsExpedition.Web.Dto
{
    public class Column
    {
        public string Name { get; set; }
        public string Data { get; set; }
        public bool Orderable { get; set; }
    }
}