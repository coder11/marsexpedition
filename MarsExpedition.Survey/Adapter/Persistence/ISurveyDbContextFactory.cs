﻿namespace MarsExpedition.Survey.Adapter.Persistence
{
    public interface ISurveyDbContextFactory
    {
        SurveyDbContext Create();
    }
}