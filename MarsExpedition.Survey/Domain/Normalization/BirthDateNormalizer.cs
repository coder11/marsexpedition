using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace MarsExpedition.Survey.Domain.Normalization
{
    /// <summary>
    /// ��������� ������������� ���� �������� �����������
    /// </summary>
    public class BirthDateNormalizer : IBirthDateNormalizer
    {
        private readonly Regex _splitterSpaceRegex = new Regex(@"[\s\.\,\:\;\-/\\]+");

        private readonly Dictionary<string, int> _verbalMonthNames = new Dictionary<string, int>
        {
            {"������", 1},
            {"���", 1},
            {"�������", 2},
            {"���", 2},
            {"����", 2},
            {"�����", 3},
            {"���", 3},
            {"������", 4},
            {"���", 4},
            {"���", 5},
            {"����", 6},
            {"����", 7},
            {"�������", 8},
            {"���", 8},
            {"��������", 9},
            {"����", 9},
            {"�������", 10},
            {"���", 10},
            {"������", 11},
            {"���", 11},
            {"���", 11},
            {"�������", 12},
            {"���", 12},
        };

        private readonly HashSet<string> _yearSuffixes = new HashSet<string> { "�", "����" };

        /// <summary>
        /// ����������� ���� ��������, ��������� �� ������ � ����� ����.
        /// ���� ���� �������� ������������� ����� - ���������� true, � �������������� ����� �������� � result.
        /// ���� ���� �������� ������������� ���������� - ���������� false. �������� result �� ����������.
        /// </summary>
        /// <param name="birthDate">���� �������� �����������</param>
        /// <param name="result">��������������� ���� �������� � ������� ������� ������������</param>
        /// <returns>true, ���� ���� �������� ���� �������������, ����� false</returns>
        /// <remarks>
        /// �������������� �������:
        /// x.x.19xx 
        /// 0x.0x.19xx
        /// xx.xx.19xx
        /// 19xx - ������ ���
        /// xx - ������ ���
        /// xx.19xx - �����, ���
        /// 
        /// ��� ���� �������������� ����������� ����� ������, �������, ����� - " " "," "." ":" "-" "/" "\"
        /// ����� ����� ���� ������� �������
        /// </remarks>
        public bool TryNormalize(string birthDate, out DateTime result)
        {
            result = default(DateTime);
            var dateParts = _splitterSpaceRegex
                .Split(birthDate)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToList();

            if (_yearSuffixes.Contains(dateParts.Last()))
            {
                dateParts.RemoveAt(dateParts.Count - 1);
            }

            DateTime? res = null;
            if (dateParts.Count == 1)
            {
                res = FromYearMonthDay(dateParts[0]);
            }
            else if (dateParts.Count == 2)
            {
                res = FromYearMonthDay(dateParts[1], dateParts[0]);
            }
            else if (dateParts.Count == 3)
            {
                res = FromYearMonthDay(dateParts[2], dateParts[1], dateParts[0]);
            }

            if (!res.HasValue)
                return false;

            result = res.Value;
            return true;
        }

        private DateTime? FromYearMonthDay(string yearRaw, string monthRaw = null, string dayRaw = null)
        {
            var year = GetYear(yearRaw);
            if (year == null)
                return null;

            if (year < 100)
            {
                year += 1900;
            }

            var month = monthRaw == null ? 1 : GetMonth(monthRaw);
            if (month == null)
                return null;

            var day = dayRaw == null ? 1 : GetDay(dayRaw);
            if (day == null)
                return null;

            // possible EN_US culture style formattin (11/19/1988)
            if (month > 12 && day < 12)
            {
                var tmp = day;
                day = month;
                month = tmp;
            }

            try
            {
                return new DateTime(year.Value, month.Value, day.Value);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                return null;
            }
        }

        private int? GetYear(string year)
        {
            var trailingYearSuffix = _yearSuffixes.FirstOrDefault(year.EndsWith);
            if (trailingYearSuffix != null)
            {
                year = year.Remove(year.Length - trailingYearSuffix.Length);
            }

            int res;
            return int.TryParse(year, out res) ? res : (int?) null;
        }

        private int? GetMonth(string month)
        {
            int ret;
            if (_verbalMonthNames.TryGetValue(month, out ret))
                return ret;

            if (int.TryParse(month, out ret))
                return ret;

            return null;
        }

        private int? GetDay(string day)
        {
            int res;
            return int.TryParse(day, out res) ? res : (int?)null;
        }
    }
}