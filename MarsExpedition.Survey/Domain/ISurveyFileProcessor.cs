using System;
using System.IO;
using System.Threading.Tasks;

namespace MarsExpedition.Survey.Domain
{
    /// <summary>
    /// ��������� ����������� ������ � ������� ����� ������������ �� ����� �� ����
    /// </summary>
    public interface ISurveyFileProcessor
    {
        /// <summary>
        /// ���������� ���������� ����
        /// </summary>
        /// <param name="file">�������� ������ ����� � ��������� ������� ������������</param>
        /// <param name="stream">�������� ����� � ������� �� �����. ������������� ���������� �� dispose ������. ����������� �� ������ dispose'��� �����</param>
        /// <returns>����� � ���������� �����</returns>
        Task ProcessFileAsync(SurveyFile file, Stream stream);

        /// <summary>
        /// �������, ������� ������ ���� ������� ��� ��������� ��������� ��������� �����
        /// </summary>
        event EventHandler<FileProcessingUpdateEvent> FileProcessingUpdate;
    }
}