﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MarsExpedition.Survey.Application;
using MarsExpedition.Survey.Domain;
using MarsExpedition.Survey.Infrastructure;
using NUnit.Framework;
using SimpleInjector;

namespace MarsExpedition.Survey.Tests.Domain
{
    [TestFixture]
    public class FileProcessingTests
    {
        private readonly Container _container;

        public FileProcessingTests()
        {
            _container = Ioc.MakeSurveyContainer();
            _container.Register<ISurveyFileRepository, FileRepo>();
        }

        [Test]
        public void Given_a_valid_csv_file__survey_file_processor_should_process_whole_file()
        {
            // Arrange
            var processor = _container.GetInstance<SurveyFileProcessor>();
            var events = new List<FileProcessingUpdateEvent>();
            processor.FileProcessingUpdate += (sender, @event) => events.Add(@event);

            // Act
            var content = new MemoryStream(Encoding.UTF8.GetBytes(ValidCsv));
            var file = new SurveyFile(size: content.Length);
            processor.ProcessFileAsync(file, content).Wait();

            // Assert
            Assert.That(file.ProcessingStatus, Is.EqualTo(SurveyFileProcessingStatus.Done));
            Assert.That(file.Records.Count, Is.EqualTo(3));
            Assert.That(events.Count, Is.EqualTo(file.Records.Count + 1));
            Assert.That(events.Select(x => x.Progress), Is.Ordered.Ascending);
            var processing = events.Take(events.Count - 1);
            var done = events.Last();
            Assert.That(processing.Select(x => x.Status), Is.All.EqualTo(SurveyFileProcessingStatus.Started));
            Assert.That(done.Status, Is.EqualTo(SurveyFileProcessingStatus.Done));
            Assert.That(done.Progress, Is.EqualTo(100));
            var record = file.Records.First();
            Assert.That(record.Name, Is.EqualTo("  Иван Иванович Иванов"));
            Assert.That(record.BirthDate, Is.EqualTo("  11.11.1990"));
            Assert.That(record.Email, Is.EqualTo("  est@test.comt"));
            Assert.That(record.MobilePhoneNumber, Is.EqualTo("  +79001233456"));
            Assert.That(record.NormalizedName, Is.EqualTo("Иван Иванович Иванов"));
            Assert.That(record.NormalizedBirthDate, Is.EqualTo(new DateTime(1990, 11, 11)));
            Assert.That(record.NormalizedEmail, Is.EqualTo("est@test.comt"));
            Assert.That(record.NormalizedMobilePhoneNumber, Is.EqualTo("+7 900 1233456"));
        }

        [Test]
        public void Given_an_invalid_csv_file__survey_file_processor_should_process_only_valid_part()
        {
            // Arrange
            var processor = _container.GetInstance<SurveyFileProcessor>();
            var events = new List<FileProcessingUpdateEvent>();
            processor.FileProcessingUpdate += (sender, @event) => events.Add(@event);

            // Act
            var content = new MemoryStream(Encoding.Default.GetBytes(InvalidCsv));
            var file = new SurveyFile(size: content.Length);
            processor.ProcessFileAsync(file, content).Wait();

            // Assert
            Assert.That(file.ProcessingStatus, Is.EqualTo(SurveyFileProcessingStatus.Error));
            Assert.That(file.Records.Count, Is.EqualTo(3));
            Assert.That(events.Count, Is.EqualTo(file.Records.Count + 1));
            Assert.That(events.Select(x => x.Progress), Is.Ordered.Ascending);
            var processing = events.Take(events.Count - 1);
            var done = events.Last();
            Assert.That(processing.Select(x => x.Status), Is.All.EqualTo(SurveyFileProcessingStatus.Started));
            Assert.That(done.Status, Is.EqualTo(SurveyFileProcessingStatus.Error));
            Assert.That(done.Progress, Is.EqualTo(100));
        }

        [Test]
        public void Given_an_survey_file_with_error_state__survey_file_processor_should_throw_error()
        {
            // Arrange 
            var processor = _container.GetInstance<SurveyFileProcessor>();

            // Act
            var file = new SurveyFile
            {
                ProcessingStatus = SurveyFileProcessingStatus.Error
            };
            var exception = processor.ProcessFileAsync(file, new MemoryStream()).Exception;

            // Assert
            Assert.That(exception.InnerExceptions[0], Is.TypeOf<ApplicationException>());
        }

        [Test]
        public void Given_an_survey_file_with_done_state__survey_file_processor_should_throw_error()
        {
            // Arrange 
            var processor = _container.GetInstance<SurveyFileProcessor>();

            // Act
            var file = new SurveyFile
            {
                ProcessingStatus = SurveyFileProcessingStatus.Done
            };
            var exception = processor.ProcessFileAsync(file, new MemoryStream()).Exception;

            // Assert
            Assert.That(exception.InnerExceptions[0], Is.TypeOf<ApplicationException>());
        }

        [Test]
        public void Given_an_partly_processed_survey_which_we_cannot_reopen__survey_file_processor_should_throw_error()
        {
            // Arrange 
            var processor = _container.GetInstance<SurveyFileProcessor>();

            // Act
            var file = new SurveyFile(size: 2048)
            {
                ProcessingStatus = SurveyFileProcessingStatus.Started
            };
            var exception = processor.ProcessFileAsync(file, new MemoryStream()).Exception;

            // Assert
            Assert.That(exception.InnerExceptions[0], Is.TypeOf<ApplicationException>());
        }

        private const string ValidCsv = 
            "  Иван Иванович Иванов\t  11.11.1990\t  est@test.comt\t  +79001233456\r\n" +
            "Иван Иванович Иванов\t11.11.1990\test@test.comt\t+79001233456\r\n" +
            "Иван Иванович Иванов\t11.11.1990\test@test.comt\t+79001233456\r\n";

        private const string InvalidCsv =
            "Иван Иванович Иванов\t11.11.1990\test@test.comt\t+79001233456\r\n" +
            "Иван Иванович Иванов\t11.11.1990\test@test.comt\t+79001233456\r\n" +
            "Иван Иванович Иванов\t11.11.1990\test@test.comt\t+79001233456\r\n" +
            "Иван Иванович Иванов\t11.11.1990\test@test.comt\t\r\n" +
            "Иван Иванович Иванов\t11.11.1990\test@test.comt\t+79001233456\r\n";

        private class FileRepo : ISurveyFileRepository
        {
            public SurveyFile FindById(Guid id)
            {
                throw new NotImplementedException();
            }

            public void AddOrUpdate(SurveyFile file)
            {
                
            }
        }
    }
}
