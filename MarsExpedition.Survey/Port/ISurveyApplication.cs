﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using MarsExpedition.Survey.Port.Commands;

namespace MarsExpedition.Survey.Port
{
    /// <summary>
    /// Порт, абстрагирующий сценарии использования приложения, связанного с анкетами претендентов на полет на Марс
    /// </summary>
    public interface ISurveyApplication
    {
        void Execute(CreateSurveyRecordCommand command);
        void Execute(UpdateSurveyRecordCommand command);
        void Execute(DeleteSurveyRecordCommand command);
        SurveyRecordQueryResultDto Execute(QuerySurveysCommand command);
        Task Execute(ProcessFileFromStreamCommand command);
    }
}
